from PyQt5.QtWidgets import (
    QApplication,
    QLabel,
    QWidget,
    QPushButton,
    QVBoxLayout
)


def hello_world(app: QApplication) -> None:
    label = QLabel('Hello World!')
    label.show()
    app.exec()


def layouts(app: QApplication) -> None:
    window = QWidget()
    layout = QVBoxLayout()
    layout.addWidget(QPushButton('Top'))
    layout.addWidget(QPushButton('Bottom'))
    window.setLayout(layout)
    window.show()
    app.exec()


app_ = QApplication([])
layouts(app_)
