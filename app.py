import json
import logging
import os
from os.path import expanduser, join, normpath, basename
from shutil import rmtree, copytree

from PyQt5 import QtWidgets
from PyQt5.QtWidgets import (
    QMainWindow,
    QApplication,
    QGridLayout,
    QPushButton,
    QLineEdit,
    QLabel,
    QFileDialog,
    QWidget,
    QVBoxLayout,
    QMessageBox,
    QCheckBox
)


def remove_module_folder(world_folder: str) -> None:
    """
    Remove destination world folder. In case the folder doesn't exist a warning is logged.
    :param world_folder: 
    """
    logging.info(f"Deleting destination module folder: {world_folder}")
    try:
        rmtree(world_folder)
        logging.info("Deleted")
    except OSError as e:
        logging.warning(f"{e.filename} - {e.strerror}.")


def copy_subfolder_to_destination(destination_folder: str, source_folder: str, sub_folder: str) -> None:
    """
    Copy a subfolder from source to destination
    :param destination_folder:
    :param source_folder:
    :param sub_folder:
    :return:
    """
    from_path = join(source_folder, sub_folder)
    to_path = join(destination_folder, sub_folder)

    logging.info(f"Copying packs folder...")
    logging.info(f"Source: {from_path}")
    logging.info(f"Destination {to_path}")

    copytree(from_path, to_path)
    logging.info("Copied")


def copy_additional_resources(module_folder: str, world_folder: str) -> None:
    """
    Copy some additional directories from world folder to module folder
    :param module_folder:
    :param world_folder:
    :return:
    """
    excluded_folder = {'.git', 'packs', 'scenes', 'data'}
    all_folders = set(next(os.walk(world_folder))[1])
    folders = list(all_folders - excluded_folder)
    logging.info(f"Copying additional folders: {folders}")
    for folder in folders:
        copy_subfolder_to_destination(
            destination_folder=module_folder,
            source_folder=world_folder,
            sub_folder=folder
        )


def replace_in_file(input_file, search, replace):
    """
    Replace string in file
    :param input_file:
    :param search:
    :param replace:
    :return:
    """
    with open(input_file, 'r', encoding='utf-8') as f:
        new_content = f.read().replace(search, replace)
    with open(input_file, 'w', encoding='utf-8') as f:
        f.write(new_content)


def fix_packs_db_references(module_name: str, module_folder: str) -> None:
    """
    Fix world folder references in module packs db files.
    :param module_name:
    :param module_folder:
    :return:
    """
    packs_folder = join(module_folder, 'packs')
    for file in os.listdir(packs_folder):
        if file.endswith('.db'):
            module_db = join(packs_folder, file)
            # Windows replacement
            replace_in_file(module_db, f"worlds\\{module_name}", f"modules\\{module_name}")
            # Unix replacement
            replace_in_file(module_db, f"worlds/{module_name}", f"modules/{module_name}")


def create_module_json(author: str, description: str, title: str, module_name: str, world_folder: str,
                       module_folder: str) -> None:
    """
    Generate the module.json in the world folder
    :param author:
    :param description:
    :param title:
    :param module_name:
    :param world_folder:
    :param module_folder:
    :return:
    """
    source_world_json_path = join(world_folder, 'world.json')
    destination_module_json_path = join(module_folder, 'module.json')
    logging.info(f"Creating {destination_module_json_path}...")
    with open(source_world_json_path, 'r+') as world_file:
        world = json.load(world_file)

        for item in world['packs']:
            item.pop('package', None)
            item.pop('absPath', None)
            item.update({'module': module_name})
            item.update({'entity': item['type']})

    module_json = {
        "id": module_name,
        "name": module_name,
        "title": title,
        "description": description,
        "authors": [{
            "name": author,
            "flags": {}
        }],
        "version": "2.0.0",
        "coreVersion": "10.291",
        "compatibility": {
            "minimum": "10",
            "verified": "10.291"
        },
        "relationships": {
            "systems": [{
                "id": "CoC7",
                "type": "system",
                "compatibility": "0.9.2"
            }]
        },
        "packs": world['packs']
    }

    with open(destination_module_json_path, 'w') as module_file:
        json.dump(module_json, module_file, indent=4, sort_keys=True, ensure_ascii=False)
    logging.info("Done")


class QTextEditLogger(logging.Handler):
    def __init__(self, parent) -> None:
        super().__init__()
        self.widget = QtWidgets.QPlainTextEdit(parent)
        self.widget.setReadOnly(True)

    def emit(self, record) -> None:
        msg = self.format(record)
        self.widget.appendPlainText(msg)


class MainWindow(QMainWindow):

    def __init__(self) -> None:
        super().__init__()
        self.setWindowTitle("Generador de Módulo de Compendios")
        # TODO Change it for a unfixed size
        self.setFixedSize(700, 400)

        self.author_text = QLineEdit()
        self.title_text = QLineEdit()
        self.description_text = QLineEdit()

        world_file_browse = QPushButton('Buscar')
        world_file_browse.clicked.connect(self._open_world_file_dialog)
        self.world_path = QLineEdit()

        modules_file_browse = QPushButton('Buscar')
        modules_file_browse.clicked.connect(self._open_modules_file_dialog)
        self.modules_path = QLineEdit()

        self.copy_folder_check = QCheckBox("Copiar carpetas adicionales de carpeta origen (Ejemplo: imágenes)")

        log_text_box = QTextEditLogger(self)
        log_text_box.setFormatter(logging.Formatter('%(asctime)s - %(levelname)s - %(message)s'))
        logging.getLogger().addHandler(log_text_box)
        logging.getLogger().setLevel(logging.DEBUG)

        self.generate_button = QPushButton('Generar Módulo', self)
        self.generate_button.clicked.connect(self._generate_world)
        self.generate_button.setEnabled(False)

        self.vertical_layout = QVBoxLayout()

        self._create_layout('Autor:', self.author_text)
        self._create_layout('Título:', self.title_text)
        self._create_layout('Descripción:', self.description_text)
        self._create_layout('Directorio Mundo:', self.world_path, world_file_browse)
        self._create_layout('Directorio Modulos:', self.modules_path, modules_file_browse)

        check_layout = QGridLayout()
        check_layout.addWidget(self.copy_folder_check)
        self.vertical_layout.addLayout(check_layout)

        log_layout = QGridLayout()
        log_layout.addWidget(log_text_box.widget)
        self.vertical_layout.addLayout(log_layout)

        button_layout = QGridLayout()
        button_layout.addWidget(self.generate_button)
        self.vertical_layout.addLayout(button_layout)

        widget = QWidget()
        widget.setLayout(self.vertical_layout)
        self.setCentralWidget(widget)

    def _create_layout(self, label: str, *args) -> None:
        layout = QGridLayout()
        layout.addWidget(QLabel(label), 0, 0)
        for i, arg in enumerate(args):
            layout.addWidget(arg, 0, i + 1)
        self.vertical_layout.addLayout(layout)

    def _open_file_dialog(self, q_line_edit: QLineEdit) -> None:
        dir_ = QFileDialog.getExistingDirectory(
            self,
            "Seleccionar directorio", 
            expanduser("~"),
            QFileDialog.ShowDirsOnly
        )
        q_line_edit.setText(dir_)
        button_enabled = self.world_path.text() != "" and self.modules_path.text() != ""
        self.generate_button.setEnabled(button_enabled)

    def _open_world_file_dialog(self) -> None:
        self._open_file_dialog(self.world_path)

    def _open_modules_file_dialog(self) -> None:
        self._open_file_dialog(self.modules_path)

    def _generate_world(self) -> None:
        try:
            module_name = basename(normpath(self.world_path.text()))
            module_folder = join(normpath(self.modules_path.text()), module_name)
            world_folder = normpath(self.world_path.text())
            logging.info(f'module_name: {module_name}')
            logging.info(f'module_folder: {module_folder}')
            logging.info(f'world_folder: {world_folder}')

            remove_module_folder(module_folder)
            copy_subfolder_to_destination(module_folder, world_folder, 'packs')
            if self.copy_folder_check.isChecked():
                copy_additional_resources(module_folder, world_folder)
                fix_packs_db_references(module_name, module_folder)
            create_module_json(
                author=self.author_text.text(),
                description=self.description_text.text(),
                title=self.title_text.text(),
                module_name=module_name,
                world_folder=world_folder,
                module_folder=module_folder
            )
            QMessageBox.about(self, "OK", "Creación del mundo exitosa")
        except Exception as e:
            logging.exception('Error')
            QMessageBox.critical(self, "Error", "Ocurrió un error con la creación\nRevisar los logs.")


app = QApplication([])
window = MainWindow()
window.show()

app.exec()
