# [FoundryVTT] Compendium Module Generator

This application takes a world with compendiums and exports it as a Compendium Module following the instructions of this [video](https://www.youtube.com/watch?v=6PWd7AfKMwg). Thanks [Viriato139ac](https://github.com/lozanoje/)!


## Windows executable creation

```
pyinstaller.exe --noconsole --onefile --name "compendium_module_gen" app.py
```
